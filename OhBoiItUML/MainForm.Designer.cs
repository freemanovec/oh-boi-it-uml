﻿using System.Diagnostics;

namespace OhBoiItUML
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewClass = new System.Windows.Forms.Button();
            this.treeViewHierarchy = new System.Windows.Forms.TreeView();
            this.pbUML = new System.Windows.Forms.PictureBox();
            this.btnNewProperty = new System.Windows.Forms.Button();
            this.btnNewMethod = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbUML)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNewClass
            // 
            this.btnNewClass.Location = new System.Drawing.Point(13, 13);
            this.btnNewClass.Name = "btnNewClass";
            this.btnNewClass.Size = new System.Drawing.Size(75, 23);
            this.btnNewClass.TabIndex = 0;
            this.btnNewClass.Text = "New Class";
            this.btnNewClass.UseVisualStyleBackColor = true;
            this.btnNewClass.Click += new System.EventHandler(this.BtnNewClass_Click);
            // 
            // treeViewHierarchy
            // 
            this.treeViewHierarchy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Bottom));
            this.treeViewHierarchy.Location = new System.Drawing.Point(13, 43);
            this.treeViewHierarchy.Name = "treeViewHierarchy";
            this.treeViewHierarchy.Size = new System.Drawing.Size(182, 706);
            this.treeViewHierarchy.TabIndex = 1;
            this.treeViewHierarchy.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewHierarchy_AfterSelect);
            // 
            // pbUML
            // 
            this.pbUML.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbUML.Location = new System.Drawing.Point(202, 43);
            this.pbUML.Name = "pbUML";
            this.pbUML.Size = new System.Drawing.Size(785, 706);
            this.pbUML.TabIndex = 2;
            this.pbUML.TabStop = false;
            this.pbUML.MouseDown += new System.Windows.Forms.MouseEventHandler((s, e) => DragUpdate(DragState.StartedDragging, e.Location));
            this.pbUML.MouseUp += new System.Windows.Forms.MouseEventHandler((s, e) => DragUpdate(DragState.StoppedDragging, e.Location));
            this.pbUML.MouseLeave += new System.EventHandler((s, e) => DragUpdate(DragState.StoppedDragging, null));
            this.pbUML.MouseMove += new System.Windows.Forms.MouseEventHandler((s, e) => DragUpdate(DragState.Dragging, e.Location));
            this.pbUML.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            // 
            // btnNewProperty
            // 
            this.btnNewProperty.Location = new System.Drawing.Point(94, 12);
            this.btnNewProperty.Name = "btnNewProperty";
            this.btnNewProperty.Size = new System.Drawing.Size(85, 23);
            this.btnNewProperty.TabIndex = 3;
            this.btnNewProperty.Text = "New Property";
            this.btnNewProperty.UseVisualStyleBackColor = true;
            this.btnNewProperty.Click += new System.EventHandler(this.BtnNewProperty_Click);
            //
            // btnNewMethod
            //
            this.btnNewMethod.Location = new System.Drawing.Point(182, 12);
            this.btnNewMethod.Name = "btnNewMethod";
            this.btnNewMethod.Size = new System.Drawing.Size(85, 23);
            this.btnNewMethod.TabIndex = 3;
            this.btnNewMethod.Text = "New Method";
            this.btnNewMethod.UseVisualStyleBackColor = true;
            this.btnNewMethod.Click += new System.EventHandler(this.BtnNewMethod_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 761);
            this.Controls.Add(this.btnNewMethod);
            this.Controls.Add(this.btnNewProperty);
            this.Controls.Add(this.pbUML);
            this.Controls.Add(this.treeViewHierarchy);
            this.Controls.Add(this.btnNewClass);
            this.Name = "MainForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pbUML)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNewClass;
        private System.Windows.Forms.TreeView treeViewHierarchy;
        private System.Windows.Forms.PictureBox pbUML;
        private System.Windows.Forms.Button btnNewProperty;
        private System.Windows.Forms.Button btnNewMethod;
    }
}

