﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Drawing
{
    internal sealed class ComplexRectangleSeparator : IComplexRectanglePart
    {
        public float Height { get; private set; }
        public float Width { get; set; }

        public ComplexRectangleSeparator(float width, float height = 2f)
        {
            Height = height;
            Width = width;
        }

        public void DrawSelf(Graphics gphcs, PointF topLeftPosition, SizeF expectedSize) {
            gphcs.FillRectangle(
                Brushes.Black,
                new RectangleF(
                    topLeftPosition,
                    expectedSize
                    ));
        }

        public SizeF GetExpectedSize(Graphics gphcs) => new SizeF(Width, Height);
    }
}
