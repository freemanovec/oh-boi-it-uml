﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Drawing
{
    internal sealed class ComplexRectangleSpacing : IComplexRectanglePart
    {
        public float Height { get; private set; }
        private float _width { get; set; }

        public ComplexRectangleSpacing(float width, float height = 2f) {
            Height = height;
            _width = width;
        }

        public void DrawSelf(Graphics gphcs, PointF topLeftPosition, SizeF expectedSize) { }

        public SizeF GetExpectedSize(Graphics gphcs) => new SizeF(_width, Height);

    }
}
