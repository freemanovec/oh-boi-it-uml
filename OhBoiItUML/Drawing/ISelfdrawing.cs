﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Drawing
{
    internal interface ISelfdrawing
    {
        void DrawSelf(Graphics gphcs, PointF topLeftPosition, SizeF expectedSize);
        SizeF GetExpectedSize(Graphics gphcs);
    }
}
