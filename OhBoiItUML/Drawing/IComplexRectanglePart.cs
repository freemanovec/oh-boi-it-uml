﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Drawing
{
    internal interface IComplexRectanglePart : ISelfdrawing
    {
        float Height { get; }
    }
}
