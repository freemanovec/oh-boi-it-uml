﻿using OhBoiItUML.Tools;
using OhBoiItUML.UMLElements.Elemental;
using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Drawing
{
    internal sealed class ComplexRectangle : ISelfdrawing
    {

        public SizeF Size { get; private set; }

        private List<IComplexRectanglePart> _parts = new List<IComplexRectanglePart>();

        public ComplexRectangle(Class clss, Graphics gphcs)
        {
            List<Property> properties = clss.Properties;
            List<Method> methods = clss.Methods;

            float classNameHeight = 15f;
            Font classNameFont = new Font(new FontFamily("Arial"), classNameHeight, clss.TypeModifier.AsFontStyle());
            _parts.Add(new ComplexRectangleHead(clss.Name, classNameFont, classNameHeight));
            _parts.Add(new ComplexRectangleSeparator(0));
            foreach(Property prop in properties)
            {
                ComplexRectangleEntry entry = new ComplexRectangleEntry(
                    prop.DisplayedText,
                    10f,
                    prop.TypeModifier.AsFontStyle()
                    );
                _parts.Add(entry);
            }
            if (properties.Count > 0 && methods.Count > 0)
                _parts.Add(new ComplexRectangleSeparator(0));
            foreach(Method method in methods)
            {
                ComplexRectangleEntry entry = new ComplexRectangleEntry(
                    method.DisplayedText,
                    10f,
                    method.TypeModifier.AsFontStyle()
                    );
                _parts.Add(entry);
            }
            _parts.Add(new ComplexRectangleSpacing(0));

            float width = _parts.Max(el => el.GetExpectedSize(gphcs).Width);

            _parts.OfType<ComplexRectangleSeparator>().ToList().ForEach(el => el.Width = width);

            Size = new SizeF(width, _parts.Sum(el => el.GetExpectedSize(gphcs).Height));
        }

        public void DrawSelf(Graphics gphcs, PointF topLeftPosition, SizeF expectedSize)
        {
            PointF currentPosition = topLeftPosition;
            Rectangle boundingRectangle = new Rectangle(new Point((int)topLeftPosition.X, (int)topLeftPosition.Y), new Size((int)expectedSize.Width, (int)expectedSize.Height));
            gphcs.FillRectangle(Brushes.LightGray, boundingRectangle);
            foreach(IComplexRectanglePart crp in _parts)
            {
                SizeF _expectedSize = crp.GetExpectedSize(gphcs);
                crp.DrawSelf(gphcs, currentPosition, _expectedSize);
                currentPosition += new SizeF(0, _expectedSize.Height);
            }
            gphcs.DrawRectangle(Pens.Black, boundingRectangle);
        }
        
        public SizeF GetExpectedSize(Graphics gphcs) => Size;
    }
}
