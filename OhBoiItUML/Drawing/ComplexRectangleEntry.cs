﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OhBoiItUML.Drawing
{
    internal sealed class ComplexRectangleEntry : IComplexRectanglePart
    {
        public float Height { get; private set; }
        public string Text { get; private set; }
        private Font _font;

        public ComplexRectangleEntry(string text, float height = 10f, FontStyle fontStyle = FontStyle.Regular)
        {
            Text = $" {text} ";
            Height = height;
            _font = new Font(new FontFamily("Arial"), height, fontStyle);
        }

        public void DrawSelf(Graphics gphcs, PointF topLeftPosition, SizeF expectedSize)
        {
            gphcs.DrawString(Text, _font, Brushes.Black, topLeftPosition);
        }

        public SizeF GetExpectedSize(Graphics gphcs) => gphcs.MeasureString(Text, _font);

    }
}
