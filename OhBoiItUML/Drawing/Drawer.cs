﻿using OhBoiItUML.Structure;
using OhBoiItUML.UMLElements.Elemental;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using OhBoiItUML.Tools;

namespace OhBoiItUML.Drawing
{
    internal sealed class Drawer
    {

        private FontFamily _fontFamily { get; set; }
        private Font _classNameFont { get; set; }
        private Graphics _virtualGraphics = null;
        public bool IsDirty { get; private set; }
        private List<PointF> _debugPoints = new List<PointF>();
        private Size _lastKnownCanvasSize = new Size(100, 100);

        public Drawer()
        {
            _classNameFont = new Font(new FontFamily("Arial"), 26f / 1.3f);
        }

        public Image GetFrame(int canvasWidth, int canvasHeight)
        {
            Image img = new Bitmap(canvasWidth, canvasHeight);
            Graphics gphcs = Graphics.FromImage(img);
            _virtualGraphics = gphcs;
            _lastKnownCanvasSize = new Size(canvasWidth, canvasHeight);
            MutateImage(gphcs, new SizeF(canvasWidth, canvasHeight));
            string rndPath = Path.Combine(Path.GetTempPath(), "uwudump_" + Path.GetRandomFileName() + ".png");
            Debug.WriteLine($"Dumping drawn image to {rndPath}");
            using (StreamWriter sw = new StreamWriter(rndPath))
                img.Save(sw.BaseStream, ImageFormat.Png);
            IsDirty = false;
            return img;
        }

        private void MutateImage(Graphics gphcs, SizeF size)
        {
            foreach(Class clss in Repository.GetRepository<Class>())
            {
                SizeF expectedSize = clss.GetExpectedSize(gphcs);
                clss.DrawSelf(gphcs, clss.Position, expectedSize);
            }

            SizeF rectSize = new SizeF(10f, 10f);
            foreach (PointF point in _debugPoints)
                gphcs.DrawRectangle(Pens.Red, new Rectangle((int)(point.X - rectSize.Width / 2), (int)(point.Y - rectSize.Height / 2), (int)rectSize.Width, (int)rectSize.Height));
            _debugPoints.Clear();

        }

        private Dictionary<Class, PointF> CalculateClassOriginPositions(SizeF size)
        {
            PointF centerPosition = new PointF(size.Width / 2, size.Height / 2);
            float radius = Math.Min((centerPosition.X / 10 * 7), (centerPosition.Y / 10 * 7));
            Class[] classes = Repository.GetRepository<Class>().ToArray();
            Dictionary<Class, PointF> ret = new Dictionary<Class, PointF>();
            if (classes.Length == 1)
                ret.Add(classes[0], centerPosition);
            else
            {
                double angle = Math.PI * 2 / ((float)classes.Length);
                for(int i = 0; i < classes.Length; i++)
                {
                    PointF point = new PointF(
                        radius * (float)Math.Sin(angle * i),
                        radius * (float)Math.Cos(angle * i)
                        );
                    point = new PointF(point.X + centerPosition.X, point.Y + centerPosition.Y);
                    ret.Add(classes[i], point);
                }
            }
            return ret;
        }

        private bool _isDragOngoing = false;
        private PointF? _lastDragPosition = null;
        private IPositionable _draggedElement = null;
        private SizeF _draggedElementSize = new SizeF(1, 1);
        private SizeF _cursorElementDelta = new SizeF(0, 0);
        internal void OnDragStateUpdate(DragState state, PointF? position)
        {
            if (state == DragState.NotDragging || _virtualGraphics == default)
            {
                Debug.Print("Invalid drag state or no graphics, ignoring");
                return;
            }

            if (position.HasValue)
                _lastDragPosition = position;
            
            PointF cursorPos = _lastDragPosition.Value;
            _debugPoints.Add(cursorPos);

            if (!_isDragOngoing && state == DragState.StartedDragging)
            {
                // new drag
                Debug.Print("New drag");

                // let's find out what we're hovering on
                List<Class> availableClasses = Repository.GetRepository<Class>();
                Class[] safeClasses = new Class[availableClasses.Count];
                availableClasses.CopyTo(safeClasses);
                Class pointerAt = safeClasses.Reverse().FirstOrDefault(clss =>
                {
                    SizeF size = clss.GetExpectedSize(_virtualGraphics);
                    RectangleF rect = new RectangleF(clss.Position, size);
                    _cursorElementDelta = new SizeF(cursorPos.X - rect.X, cursorPos.Y - rect.Y);
                    _draggedElementSize = size;
                    return rect.Contains(cursorPos);
                });
                if (pointerAt == default)
                    return;

                // bring the dragged element to front
                Repository.RemoveFromRepo(pointerAt);
                Repository.AddToRepo(pointerAt);
                _draggedElement = pointerAt;
                _isDragOngoing = true;

            }else if (_isDragOngoing)
            {
                if(state == DragState.Dragging && _draggedElement != default)
                {
                    // drag progress
                    Debug.Print("Drag progress");

                    PointF newPosition = (cursorPos - _cursorElementDelta).ClampTo(new RectangleF(0, 0, _lastKnownCanvasSize.Width - _draggedElementSize.Width, _lastKnownCanvasSize.Height - _draggedElementSize.Height));
                    _draggedElement.Position = newPosition;
                }
                else
                {
                    // drag done
                    Debug.Print("Drag finished");

                    _isDragOngoing = false;
                    _draggedElement = null;
                }
            }

            IsDirty = true;
        }

        public void MarkDirty() => IsDirty = true;


    }
}
