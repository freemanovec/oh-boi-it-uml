﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Drawing
{
    internal sealed class ComplexRectangleHead : IComplexRectanglePart
    {


        public float Height { get; private set; }
        public string Title { get; private set; }
        private Font _font;

        public ComplexRectangleHead(string title, Font font, float height)
        {
            Title = title;
            Height = height;
            _font = font;
        }

        public void DrawSelf(Graphics gphcs, PointF topLeftPosition, SizeF expectedSize)
        {
            gphcs.DrawString(Title, _font, Brushes.Black, topLeftPosition);
        }

        public SizeF GetExpectedSize(Graphics gphcs) => gphcs.MeasureString(Title, _font);

        public SizeF GetSize(Graphics gphcs)
        {
            throw new NotImplementedException();
        }
    }
}
