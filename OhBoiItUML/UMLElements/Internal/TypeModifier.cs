﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.UMLElements.Internal
{
    internal enum TypeModifier
    {
        Normal,
        Abstract,
        Static
    }
}
