﻿using OhBoiItUML.UMLElements.Elemental;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.UMLElements.Internal
{
    internal sealed class Method : OnClassElement, IDisplayableAsText
    {

        public List<OnMethodArgument> Arguments { get; private set; } = new List<OnMethodArgument>();

        public Method(Class baseClass, string name, AccessModifier accMod, TypeModifier tpMod, string type) : base(baseClass, name, accMod, tpMod, type) {

        }

        public string DisplayedText
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                switch (AccessModifier)
                {
                    case AccessModifier.Public:
                        sb.Append("+");
                        break;
                    case AccessModifier.Private:
                        sb.Append("-");
                        break;
                    case AccessModifier.Protected:
                        sb.Append("#");
                        break;
                    default:
                        sb.Append("?");
                        break;
                }
                sb.Append(" ");
                sb.Append(Name);
                sb.Append("(");

                sb.Append(ArgumentLine);
                sb.Append(")");

                sb.Append(" : ");
                sb.Append(Type);

                return sb.ToString();
            }
        }

        private string ArgumentLine
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                List<string> displayableArguments = new List<string>(Arguments.Count);
                Arguments.ForEach(arg => displayableArguments.Add($"{arg.Type} {arg.Name}"));
                sb.Append(string.Join(", ", displayableArguments));
                return sb.ToString();
            }
        }

        public override string ToString() => $"{Name}({ArgumentLine}) ({AccessModifier} method)";

    }
}
