﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.UMLElements.Internal
{
    internal sealed class OnMethodArgument
    {
        public string Name { get; private set; }
        public string Type { get; private set; }
    }
}
