﻿using OhBoiItUML.Structure;
using OhBoiItUML.UMLElements.Elemental;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.UMLElements.Internal
{
    internal sealed class Property : OnClassElement, IDisplayableAsText
    {
               
        public Property(Class baseClass, string name, AccessModifier accMod, TypeModifier tpMod, string type) : base(baseClass, name, accMod, tpMod, type)
        {
            if (baseClass.Properties.Where(el => el.Name == name).Count() > 0)
                throw new ArgumentException($"{baseClass} already contains a '{name}' property");

            HierarchyNode asNode = new HierarchyNode(this);
            HierarchyNode parentNode = Hierarchy.SeekNode(baseClass.Name, typeof(Class));
            parentNode.ChildNodes.Add(asNode);
            baseClass.Properties.Add(this);
            Hierarchy.OnNodeTreeChanged.Invoke(this, null);
        }

        public override string ToString() => $"{Name} ({AccessModifier} property)";
        public string DisplayedText
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                switch (AccessModifier)
                {
                    case AccessModifier.Public:
                        sb.Append("+");
                        break;
                    case AccessModifier.Private:
                        sb.Append("-");
                        break;
                    case AccessModifier.Protected:
                        sb.Append("#");
                        break;
                    default:
                        sb.Append("?");
                        break;
                }
                sb.Append(" ");
                sb.Append(Name);
                sb.Append(" : ");
                sb.Append(Type);

                return sb.ToString();
            }
        }

    }
}
