﻿using OhBoiItUML.UMLElements.Elemental;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.UMLElements.Internal
{
    internal abstract class OnClassElement
    {
        public AccessModifier AccessModifier { get; protected set; }
        public TypeModifier TypeModifier { get; protected set; }
        public string Name { get; protected set; }
        public string Type { get; protected set; }
        public Class BaseClass { get; protected set; }

        internal OnClassElement(Class _baseClass, string _name, AccessModifier _accMod, TypeModifier _tpMod, string _type)
        {
            Name = _name;
            AccessModifier = _accMod;
            TypeModifier = _tpMod;
            BaseClass = _baseClass;
            Type = _type;
        }
    }
}
