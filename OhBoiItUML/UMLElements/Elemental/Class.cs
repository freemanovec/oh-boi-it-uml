﻿using OhBoiItUML.Drawing;
using OhBoiItUML.Structure;
using OhBoiItUML.Tools;
using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.UMLElements.Elemental
{
    internal sealed class Class : ISelfdrawing, IPositionable
    {

        public string Name { get; private set; }
        public TypeModifier TypeModifier { get; private set; }
        public List<Property> Properties { get; private set; } = new List<Property>();
        public List<Method> Methods { get; private set; } = new List<Method>();

        public HierarchyNode AsNode { get; private set; }

        public PointF Position { get; set; } = new PointF(0, 0);

        public Class(string name, TypeModifier typeModifier, Image image) : this(name, typeModifier, new RectangleF(new PointF(0, 0), image.Size)) { }
        public Class(string name, TypeModifier typeModifier, RectangleF availableSpace) : this(name, typeModifier)
        {
            Position = availableSpace.GetRandomPoint();
        }

        public Class(string name, TypeModifier typeModifier)
        {
            if (Repository.GetRepository<Class>().Where(el => el.Name == name).Count() > 0)
                throw new ArgumentException($"Class repository already contains '{name}'");
            Name = name;
            TypeModifier = typeModifier;
            Repository.AddToRepo(this);
            AsNode = new HierarchyNode(this);
            Hierarchy.Nodes.Add(AsNode);
        }

        public override string ToString() => $"Class '{Name}'";

        public SizeF GetExpectedSize(Graphics gphcs) => new ComplexRectangle(this, gphcs).Size;

        public void DrawSelf(Graphics gphcs, PointF topLeftPosition, SizeF expectedSize) => new ComplexRectangle(this, gphcs).DrawSelf(gphcs, topLeftPosition, expectedSize);


    }
}
