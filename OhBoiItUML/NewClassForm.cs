﻿using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OhBoiItUML
{
    public partial class NewClassForm : Form
    {

        private MainForm _baseForm;

        public NewClassForm(MainForm baseForm)
        {
            InitializeComponent();
            _baseForm = baseForm;
            btnCancel.Click += (s, e) => Close();

            FillRealizations(cbRealization.Items);
            cbRealization.SelectedIndex = 0;
        }

        private void FillRealizations(ComboBox.ObjectCollection collection)
        {
            collection.Clear();
            collection.AddRange(Enum.GetNames(typeof(TypeModifier)));
        }

        internal void AddOnNewClassCallback(EventHandler<NewClassArgs> eh) => btnCreate.Click += (s, e) =>
        {
            string cntnd = tbNewClassName.Text;
            TypeModifier tpMod = (TypeModifier)Enum.Parse(typeof(TypeModifier), cbRealization.SelectedItem.ToString());
            NewClassArgs args = new NewClassArgs
            {
                Name = cntnd,
                TypeModifier = tpMod
            };
            if (!string.IsNullOrEmpty(cntnd))
                eh?.Invoke(this, args);
            Close();
        };

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            _baseForm.SwitchBtnStateNewClass(true);
        }
    }

    internal class NewClassArgs : EventArgs
    {
        public string Name { get; set; }
        public TypeModifier TypeModifier { get; set; }
    }
}
