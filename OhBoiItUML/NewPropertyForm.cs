﻿using OhBoiItUML.Structure;
using OhBoiItUML.UMLElements.Elemental;
using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OhBoiItUML
{
    public partial class NewPropertyForm : Form
    {

        private readonly MainForm _baseForm;
        private readonly Class _parentClass;
        private const string CUSTOM_TYPE_DESIGNATOR = "Custom type";

        internal NewPropertyForm(Class parentClass, MainForm baseForm)
        {
            InitializeComponent();

            _baseForm = baseForm;
            _parentClass = parentClass;

            tbCustomType.Hide();
            lblCustomType.Hide();

            btnCancel.Click += (s, e) => Close();

            FillSelectableTypes(cbNewPropertyType.Items);
            FillAccessibilityLevels(cbAccessLevel.Items);
            FillRealizations(cbRealization.Items);

            cbNewPropertyType.SelectedIndex = 0;
            cbAccessLevel.SelectedIndex = 0;
            cbRealization.SelectedIndex = 0;

        }

        private void FillSelectableTypes(ComboBox.ObjectCollection collection)
        {
            collection.Clear();

            // add some base types
            collection.Add("String");
            collection.Add("Integer");

            // add created classes
            Repository.GetRepository<Class>().ForEach(el => collection.Add(el.Name));

            // add the option to select a custom one
            collection.Add(CUSTOM_TYPE_DESIGNATOR);
        }

        private void FillAccessibilityLevels(ComboBox.ObjectCollection collection)
        {
            collection.Clear();
            collection.AddRange(Enum.GetNames(typeof(AccessModifier)));
        }

        private void FillRealizations(ComboBox.ObjectCollection collection)
        {
            collection.Clear();
            collection.AddRange(Enum.GetNames(typeof(TypeModifier)));
        }

        internal void AddOnNewClassCallback(EventHandler<NewPropertyArgs> eh) => btnCreate.Click += (s, e) =>
        {
            string propertyName = tbNewPropertyName.Text;
            if (string.IsNullOrEmpty(propertyName))
                return;
            string selectedType = cbNewPropertyType.SelectedItem.ToString();
            if(selectedType == CUSTOM_TYPE_DESIGNATOR)
            {
                string customTypeName = tbCustomType.Text;
                if(string.IsNullOrEmpty(customTypeName))
                {
                    MessageBox.Show("No custom type name specified", "Unable", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                selectedType = customTypeName;
            }

            AccessModifier selAccMod = (AccessModifier)Enum.Parse(typeof(AccessModifier), cbAccessLevel.SelectedItem.ToString());
            TypeModifier tpMod = (TypeModifier)Enum.Parse(typeof(TypeModifier), cbRealization.SelectedItem.ToString());
            NewPropertyArgs args = new NewPropertyArgs
            {
                Name = propertyName,
                Type = selectedType,
                AccessModifier = selAccMod,
                TypeModifier = tpMod,
                ParentClass = _parentClass
            };
            eh?.Invoke(this, args);

            Close();
        };

        private void NewPropertyForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void CbNewPropertyType_SelectedValueChanged(object sender, EventArgs e)
        {
            string newTypeName = cbNewPropertyType.SelectedItem.ToString();
            if(newTypeName == CUSTOM_TYPE_DESIGNATOR)
            {
                lblCustomType.Show();
                tbCustomType.Show();
                btnCreate.Location = new Point(80, 162);
                btnCancel.Location = new Point(161, 162);
                Height = 231;
            }
            else
            {
                lblCustomType.Hide();
                tbCustomType.Hide();
                btnCreate.Location = new Point(80, 129);
                btnCancel.Location = new Point(161, 129);
                Height = 198;
            }
        }

    }

    internal class NewPropertyArgs : EventArgs
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public AccessModifier AccessModifier { get; set; }
        public TypeModifier TypeModifier { get; set; }
        public Class ParentClass { get; set; }
    }
}
