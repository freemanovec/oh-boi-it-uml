﻿namespace OhBoiItUML
{
    partial class NewMethodForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.cbAccessLevel = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbNewPropertyType = new System.Windows.Forms.ComboBox();
            this.tbNewMethodName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.arg1Type = new System.Windows.Forms.ComboBox();
            this.arg1Name = new System.Windows.Forms.TextBox();
            this.arg2Name = new System.Windows.Forms.TextBox();
            this.arg2Type = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cbRealization = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Acc. lvl.";
            // 
            // cbAccessLevel
            // 
            this.cbAccessLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAccessLevel.FormattingEnabled = true;
            this.cbAccessLevel.Location = new System.Drawing.Point(83, 32);
            this.cbAccessLevel.Name = "cbAccessLevel";
            this.cbAccessLevel.Size = new System.Drawing.Size(157, 21);
            this.cbAccessLevel.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Type";
            // 
            // cbNewPropertyType
            // 
            this.cbNewPropertyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNewPropertyType.FormattingEnabled = true;
            this.cbNewPropertyType.Location = new System.Drawing.Point(55, 57);
            this.cbNewPropertyType.Name = "cbNewPropertyType";
            this.cbNewPropertyType.Size = new System.Drawing.Size(185, 21);
            this.cbNewPropertyType.TabIndex = 14;
            // 
            // tbNewMethodName
            // 
            this.tbNewMethodName.Location = new System.Drawing.Point(55, 6);
            this.tbNewMethodName.Name = "tbNewMethodName";
            this.tbNewMethodName.Size = new System.Drawing.Size(185, 20);
            this.tbNewMethodName.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Aguments:";
            // 
            // arg1Type
            // 
            this.arg1Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.arg1Type.FormattingEnabled = true;
            this.arg1Type.Location = new System.Drawing.Point(12, 130);
            this.arg1Type.Name = "arg1Type";
            this.arg1Type.Size = new System.Drawing.Size(67, 21);
            this.arg1Type.TabIndex = 19;
            // 
            // arg1Name
            // 
            this.arg1Name.Location = new System.Drawing.Point(86, 130);
            this.arg1Name.Name = "arg1Name";
            this.arg1Name.Size = new System.Drawing.Size(154, 20);
            this.arg1Name.TabIndex = 20;
            // 
            // arg2Name
            // 
            this.arg2Name.Location = new System.Drawing.Point(86, 157);
            this.arg2Name.Name = "arg2Name";
            this.arg2Name.Size = new System.Drawing.Size(154, 20);
            this.arg2Name.TabIndex = 22;
            // 
            // arg2Type
            // 
            this.arg2Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.arg2Type.FormattingEnabled = true;
            this.arg2Type.Location = new System.Drawing.Point(12, 157);
            this.arg2Type.Name = "arg2Type";
            this.arg2Type.Size = new System.Drawing.Size(67, 21);
            this.arg2Type.TabIndex = 21;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(165, 183);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 24;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(84, 183);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 23;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.BtnCreate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Realization";
            // 
            // cbRealization
            // 
            this.cbRealization.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRealization.FormattingEnabled = true;
            this.cbRealization.Location = new System.Drawing.Point(84, 84);
            this.cbRealization.Name = "cbRealization";
            this.cbRealization.Size = new System.Drawing.Size(157, 21);
            this.cbRealization.TabIndex = 25;
            // 
            // NewMethodForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 218);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbRealization);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.arg2Name);
            this.Controls.Add(this.arg2Type);
            this.Controls.Add(this.arg1Name);
            this.Controls.Add(this.arg1Type);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbAccessLevel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbNewPropertyType);
            this.Controls.Add(this.tbNewMethodName);
            this.Controls.Add(this.label1);
            this.Name = "NewMethodForm";
            this.Text = "NewMethodForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbAccessLevel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbNewPropertyType;
        private System.Windows.Forms.TextBox tbNewMethodName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox arg1Type;
        private System.Windows.Forms.TextBox arg1Name;
        private System.Windows.Forms.TextBox arg2Name;
        private System.Windows.Forms.ComboBox arg2Type;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbRealization;
    }
}