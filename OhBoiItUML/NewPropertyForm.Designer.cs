﻿namespace OhBoiItUML
{
    partial class NewPropertyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbNewPropertyName = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.cbNewPropertyType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCustomType = new System.Windows.Forms.TextBox();
            this.lblCustomType = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbAccessLevel = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbRealization = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // tbNewPropertyName
            // 
            this.tbNewPropertyName.Location = new System.Drawing.Point(55, 13);
            this.tbNewPropertyName.Name = "tbNewPropertyName";
            this.tbNewPropertyName.Size = new System.Drawing.Size(185, 20);
            this.tbNewPropertyName.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(161, 161);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(80, 161);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 4;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            // 
            // cbNewPropertyType
            // 
            this.cbNewPropertyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNewPropertyType.FormattingEnabled = true;
            this.cbNewPropertyType.Location = new System.Drawing.Point(55, 98);
            this.cbNewPropertyType.Name = "cbNewPropertyType";
            this.cbNewPropertyType.Size = new System.Drawing.Size(185, 21);
            this.cbNewPropertyType.TabIndex = 6;
            this.cbNewPropertyType.SelectedValueChanged += new System.EventHandler(this.CbNewPropertyType_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Type";
            // 
            // tbCustomType
            // 
            this.tbCustomType.Location = new System.Drawing.Point(83, 125);
            this.tbCustomType.Name = "tbCustomType";
            this.tbCustomType.Size = new System.Drawing.Size(157, 20);
            this.tbCustomType.TabIndex = 9;
            // 
            // lblCustomType
            // 
            this.lblCustomType.AutoSize = true;
            this.lblCustomType.Location = new System.Drawing.Point(12, 128);
            this.lblCustomType.Name = "lblCustomType";
            this.lblCustomType.Size = new System.Drawing.Size(65, 13);
            this.lblCustomType.TabIndex = 8;
            this.lblCustomType.Text = "Custom type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Acc. lvl.";
            // 
            // cbAccessLevel
            // 
            this.cbAccessLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAccessLevel.FormattingEnabled = true;
            this.cbAccessLevel.Location = new System.Drawing.Point(83, 39);
            this.cbAccessLevel.Name = "cbAccessLevel";
            this.cbAccessLevel.Size = new System.Drawing.Size(157, 21);
            this.cbAccessLevel.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Realization";
            // 
            // cbRealization
            // 
            this.cbRealization.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRealization.FormattingEnabled = true;
            this.cbRealization.Location = new System.Drawing.Point(83, 69);
            this.cbRealization.Name = "cbRealization";
            this.cbRealization.Size = new System.Drawing.Size(157, 21);
            this.cbRealization.TabIndex = 12;
            // 
            // NewPropertyForm
            // 
            this.AcceptButton = this.btnCreate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(248, 196);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbRealization);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbAccessLevel);
            this.Controls.Add(this.tbCustomType);
            this.Controls.Add(this.lblCustomType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbNewPropertyType);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.tbNewPropertyName);
            this.Controls.Add(this.label1);
            this.Name = "NewPropertyForm";
            this.Text = "NewPropertyForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NewPropertyForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNewPropertyName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.ComboBox cbNewPropertyType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbCustomType;
        private System.Windows.Forms.Label lblCustomType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbAccessLevel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbRealization;
    }
}