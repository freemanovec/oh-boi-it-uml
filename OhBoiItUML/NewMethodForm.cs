﻿using OhBoiItUML.Structure;
using OhBoiItUML.UMLElements.Elemental;
using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OhBoiItUML
{
    public partial class NewMethodForm : Form
    {
        private readonly MainForm _baseForm;
        private readonly Class _parentClass;
        private readonly List<Control> _argumentControls = new List<Control>();
        private readonly List<KeyValuePair<string, string>> _argumentList = new List<KeyValuePair<string, string>>(); // holds the currently set arguments, (type, name)

        private float
            _verticalDistanceToFirstArgument,
            _verticalDistanceBetweenArguments,
            _verticalWindowSizeOffsetFromLastArgument,
            _verticalButtonOffsetFromLastArgument,
            _horizontalOffsetType,
            _horizontalOffsetName;

        internal NewMethodForm(Class parentClass, MainForm baseForm)
        {
            InitializeComponent();

            _baseForm = baseForm;
            _parentClass = parentClass;

            btnCancel.Click += (s, e) => Close();

            FillSelectableTypes(cbNewPropertyType.Items);
            FillAccessibilityLevels(cbAccessLevel.Items);
            FillRealizations(cbRealization.Items);

            _verticalWindowSizeOffsetFromLastArgument = Height - arg2Name.Location.Y;
            _verticalButtonOffsetFromLastArgument = btnCreate.Location.Y - arg2Name.Location.Y;
            _verticalDistanceToFirstArgument = arg1Name.Location.Y;
            _verticalDistanceBetweenArguments = arg2Name.Location.Y - arg1Name.Location.Y;
            _horizontalOffsetType = arg1Type.Location.X;
            _horizontalOffsetName = arg1Name.Location.X;

            arg1Type.Dispose();
            arg1Name.Dispose();
            arg2Type.Dispose();
            arg2Name.Dispose();

            RegenerateArgumentView(5);
        }

        private object _regenerationLock = new object();
        private void RegenerateArgumentView(int argc)
        {
            lock (_regenerationLock)
            {
                if (_argumentControls.Count % 2 != 0)
                    return;
                _argumentList.Clear();
                for(int i = 0; i < _argumentControls.Count - 1; i += 2)
                {
                    string type = (_argumentControls[i] as ComboBox).SelectedItem.ToString();
                    string name = (_argumentControls[i + 1] as TextBox).Text;
                    KeyValuePair<string, string> kvp = new KeyValuePair<string, string>(type, name);
                    _argumentList.Add(kvp);
                }
                foreach (Control ctrl in _argumentControls)
                    ctrl.Dispose();
                _argumentControls.Clear();
                for(int i = 0; i < argc; i++)
                {
                    // create a new pair
                    float positionY = _verticalDistanceToFirstArgument + i * _verticalDistanceBetweenArguments;

                    ComboBox cbType = new ComboBox();
                    cbType.Location = new Point((int)_horizontalOffsetType, (int)positionY);
                    FillSelectableTypes(cbType.Items);
                    TextBox tbName = new TextBox();
                    tbName.Location = new Point((int)_horizontalOffsetName, (int)positionY);
                    if(i < _argumentList.Count)
                    {
                        string type = _argumentList[i].Key;
                        string name = _argumentList[i].Value;
                        cbType.SelectedIndex = cbType.Items.IndexOf(type);
                        tbName.Text = name;
                    }
                    Controls.Add(cbType);
                    Controls.Add(tbName);
                    _argumentControls.Add(cbType);
                    _argumentControls.Add(tbName);
                }
            }
        }

        internal void AddOnNewMethodCallback(EventHandler<NewMethodArgs> eh) => btnCreate.Click += (s, e) =>
        {
            string methodName = tbNewMethodName.Text;
            if (string.IsNullOrEmpty(methodName))
                return;
            string selectedType = cbNewPropertyType.SelectedItem.ToString();

            AccessModifier selAccMod = (AccessModifier)Enum.Parse(typeof(AccessModifier), cbAccessLevel.SelectedItem.ToString());
            TypeModifier tpMod = (TypeModifier)Enum.Parse(typeof(TypeModifier), cbRealization.SelectedItem.ToString());
        };

        private void BtnCreate_Click(object sender, EventArgs e)
        {

        }

        private void FillSelectableTypes(ComboBox.ObjectCollection collection)
        {
            collection.Clear();

            // add some base types
            collection.Add("String");
            collection.Add("Integer");

            // add created classes
            Repository.GetRepository<Class>().ForEach(el => collection.Add(el.Name));

        }

        private void FillAccessibilityLevels(ComboBox.ObjectCollection collection)
        {
            collection.Clear();
            collection.AddRange(Enum.GetNames(typeof(AccessModifier)));
        }

        private void FillRealizations(ComboBox.ObjectCollection collection)
        {
            collection.Clear();
            collection.AddRange(Enum.GetNames(typeof(TypeModifier)));
        }
    }

    internal class NewMethodArgs : EventArgs
    {
        public string Name;
        public string Type;
        public List<KeyValuePair<string, string>> Arguments;
        public AccessModifier AccessModifier { get; set; }
        public TypeModifier TypeModifier { get; set; }
        public Class ParentClass { get; set; }
    }
}
