﻿using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Tools
{
    internal static class Extensions
    {
        public static PointF GetRandomPoint(this RectangleF rect, Random rand = null)
        {
            if (rand == null)
                rand = new Random();
            return new PointF(rect.Left + (float)(rand.NextDouble() * rect.Width), rect.Top + (float)(rand.NextDouble() * rect.Height));
        }

        public static PointF ClampTo(this PointF originalPosition, RectangleF boundary)
        {
            return new PointF(
                originalPosition.X < boundary.X ? 0 : (originalPosition.X > boundary.X + boundary.Width ? boundary.X + boundary.Width : originalPosition.X),
                originalPosition.Y < boundary.Y ? 0 : (originalPosition.Y > boundary.Y + boundary.Height ? boundary.Y + boundary.Width : originalPosition.Y)
                );
        }

        public static FontStyle AsFontStyle(this TypeModifier tm)
        {
            switch (tm)
            {
                case TypeModifier.Abstract:
                    return FontStyle.Italic;
                case TypeModifier.Static:
                    return FontStyle.Underline;
                default:
                    return FontStyle.Regular;
            }
        }
    }
}
