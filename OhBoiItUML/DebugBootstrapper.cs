﻿using OhBoiItUML.UMLElements.Elemental;
using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML
{
    internal sealed class DebugBootstrapper
    {

        internal void Bootstrap(MainForm mainForm)
        {
#if DEBUG
            // create two base classes
            Class c1 = new Class("User", TypeModifier.Normal);
            Property c1p1 = new Property(c1, "Name", AccessModifier.Public, TypeModifier.Normal, "String");
            Property c1p2 = new Property(c1, "Surname", AccessModifier.Public, TypeModifier.Normal, "String");
            Property c1p4 = new Property(c1, "Owner", AccessModifier.Protected, TypeModifier.Static, "Administrator");
            Property c1p3 = new Property(c1, "Age", AccessModifier.Private, TypeModifier.Abstract, "Integer");

            Class c2 = new Class("Administrator", TypeModifier.Static);
            Property c2p1 = new Property(c2, "Nick", AccessModifier.Public, TypeModifier.Normal, "String");
            Property c2p2 = new Property(c2, "Enabled", AccessModifier.Private, TypeModifier.Normal, "Bool");
#endif
        }

    }
}
