﻿using OhBoiItUML.Drawing;
using OhBoiItUML.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OhBoiItUML
{
    public partial class MainForm : Form
    {

        private Drawer _drawer = new Drawer();
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private delegate void _invocationTarget();
        
        private UMLElements.Elemental.Class SelectedClass
        {
            get
            {
                TreeNode selectedNode = treeViewHierarchy.SelectedNode;
                if (selectedNode == null)
                    return null;
                if (!Repository.NodeMapping.ContainsKey(selectedNode))
                    return null;
                if (Repository.NodeMapping[selectedNode] is UMLElements.Elemental.Class clss)
                    return clss;
                if (Repository.NodeMapping[selectedNode] is UMLElements.Internal.OnClassElement oce)
                    return oce.BaseClass;
                return null;
            }
        }

        public MainForm()
        {
            InitializeComponent();

            Hierarchy.OnNodeTreeChanged += (s, e) =>
            {
                RebuildHierarchy();
                RegenerateDiagram();
            };
            
            RecheckButtonAbilities();
            RegenerateDiagram();

            /*pbUML.Paint += (s, e) =>
            {
                if (_drawer.IsDirty)
                    RegenerateDiagram();
            };*/

            new Thread(() =>
            {
                while (!_cancellationTokenSource.IsCancellationRequested)
                {
                    Thread.Sleep(10);
                    if (_drawer.IsDirty)
                        Invoke(new _invocationTarget(() => {
                            RegenerateDiagram();
                        }));
                }
            }).Start();

            {
                (new DebugBootstrapper()).Bootstrap(this);
            }
        }

        ~MainForm() => _cancellationTokenSource.Cancel();

        private void RebuildHierarchy()
        {
            TreeNodeCollection nodes = treeViewHierarchy.Nodes;
            nodes.Clear();
            Repository.NodeMapping.Clear();
            foreach (HierarchyNode node in Hierarchy.Nodes)
                AddNodeToTreeView(nodes, node);
            treeViewHierarchy.ExpandAll();
        }

        private void RegenerateDiagram()
        {
            pbUML.Image = _drawer.GetFrame(pbUML.Width, pbUML.Height);
            pbUML.Refresh();
        }

        private void RecheckButtonAbilities()
        {
            btnNewProperty.Enabled = SelectedClass != null;
        }

        private void AddNodeToTreeView(TreeNodeCollection collection, HierarchyNode currentNode)
        {
            string textualRepresentation = currentNode.Contained.ToString();
            TreeNode newNode = new TreeNode(textualRepresentation);
            Repository.NodeMapping[newNode] = currentNode.Contained;
            collection.Add(newNode);
            foreach (HierarchyNode node in currentNode.ChildNodes)
                AddNodeToTreeView(newNode.Nodes, node);
        }

        private void BtnNewClass_Click(object sender, EventArgs e)
        {
            NewClassForm ncf = new NewClassForm(this);
            SwitchBtnStateNewClass(false);
            ncf.AddOnNewClassCallback((s, newClassArgs) =>
            {
                try
                {
                    UMLElements.Elemental.Class cls = new UMLElements.Elemental.Class(newClassArgs.Name, newClassArgs.TypeModifier);
                }catch(ArgumentException agx)
                {
                    MessageBox.Show(agx.Message, "Unable", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            });
            ncf.Show();
        }

        public void SwitchBtnStateNewClass(bool state) => btnNewClass.Enabled = state;

        private void BtnNewProperty_Click(object sender, EventArgs e)
        {
            UMLElements.Elemental.Class selectedClass = SelectedClass;
            if (selectedClass == null)
                return;
            NewPropertyForm npf = new NewPropertyForm(selectedClass, this);
            SwitchBtnStateNewProperty(false);
            npf.AddOnNewClassCallback((s, newPropertyArgs) =>
            {
                try
                {
                    UMLElements.Internal.Property newProp = new UMLElements.Internal.Property(newPropertyArgs.ParentClass, newPropertyArgs.Name, newPropertyArgs.AccessModifier, newPropertyArgs.TypeModifier, newPropertyArgs.Type);
                }
                catch (ArgumentException agx)
                {
                    MessageBox.Show(agx.Message, "Unable", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            });
            npf.Show();
        }

        private void BtnNewMethod_Click(object sender, EventArgs e)
        {
            UMLElements.Elemental.Class selectedClass = SelectedClass;
            if (selectedClass == null)
                return;
            NewMethodForm nmf = new NewMethodForm(selectedClass, this);
            SwitchBtnStateNewMethod(false);
            nmf.AddOnNewMethodCallback((s, newMethodArgs) =>
            {
                try
                {

                }
                catch (ArgumentException agx)
                {
                    MessageBox.Show(agx.Message, "Unable", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            });
            nmf.Show();
        }

        public void SwitchBtnStateNewProperty(bool state) => btnNewProperty.Enabled = state;

        public void SwitchBtnStateNewMethod(bool state) => btnNewMethod.Enabled = state;

        private void TreeViewHierarchy_AfterSelect(object sender, TreeViewEventArgs e) => RecheckButtonAbilities();

        private void DragUpdate(DragState state, PointF? position) => _drawer.OnDragStateUpdate(state, position);

        

    }

    internal enum DragState
    {
        NotDragging,
        StartedDragging,
        Dragging,
        StoppedDragging
    }
}
