﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Structure
{
    internal static class Repository
    {

        private static Dictionary<Type, List<object>> _repositories = new Dictionary<Type, List<object>>();

        public static Dictionary<System.Windows.Forms.TreeNode, object> NodeMapping { get; private set; } = new Dictionary<System.Windows.Forms.TreeNode, object>();
            
        public static List<StoredType> GetRepository<StoredType>()
        {
            Type t = typeof(StoredType);
            if (!_repositories.ContainsKey(t))
                _repositories.Add(t, new List<object>());
            List<object> rtrvd = _repositories[t];
            return rtrvd.Cast<StoredType>().ToList();
        }

        public static void AddToRepo<T>(T obj)
        {
            Type t = typeof(T);
            if (!_repositories.ContainsKey(t))
                _repositories.Add(t, new List<object>());
            _repositories[t].Add(obj);
        }

        public static void InsertToRepo<T>(int index, T obj)
        {
            Type t = typeof(T);
            if (!_repositories.ContainsKey(t))
                _repositories.Add(t, new List<object>());
            _repositories[t].Insert(index, obj);
        }

        public static void RemoveFromRepo<T>(T obj)
        {
            Type t = typeof(T);
            if (_repositories.ContainsKey(t))
                _repositories[t].Remove(obj);
        }

    }
}
