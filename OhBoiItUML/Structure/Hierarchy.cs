﻿using OhBoiItUML.UMLElements.Elemental;
using OhBoiItUML.UMLElements.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Structure
{
    internal static class Hierarchy
    {

        public static BindingList<HierarchyNode> Nodes { get; private set; } = new BindingList<HierarchyNode>();

        public static EventHandler OnNodeTreeChanged = (s, e) => { };


        static Hierarchy() => Nodes.ListChanged += (s, e) => OnNodeTreeChanged(s, e);

        public static object Seek(string name, Type type)
        {
            foreach(HierarchyNode node in Nodes)
            {
                object sought = Seek(node, name, type);
                if (sought != null)
                    return sought;
            }

            return null;
        }

        private static object Seek(HierarchyNode currentNode, string name, Type type)
        {
            if (currentNode.ContainedType == type && currentNode.Contained.GetType() == type)
            {
                switch (currentNode.Contained)
                {
                    case Class clss:
                        if (clss.Name == name)
                            return clss;
                        break;
                    case OnClassElement oce:
                        if (oce.Name == name)
                            return oce;
                        break;
                }
            }

            foreach(HierarchyNode node in currentNode.ChildNodes)
            {
                object sought = Seek(node, name, type);
                if (sought != null)
                    return sought;
            }

            return null;
        }

        public static HierarchyNode SeekNode(string name, Type type)
        {
            foreach (HierarchyNode node in Nodes)
            {
                HierarchyNode sought = SeekNode(node, name, type);
                if (sought != null)
                    return sought;
            }

            return null;
        }

        private static HierarchyNode SeekNode(HierarchyNode currentNode, string name, Type type)
        {
            if (currentNode.ContainedType == type && currentNode.Contained.GetType() == type)
            {
                switch (currentNode.Contained)
                {
                    case Class clss:
                        if (clss.Name == name)
                            return currentNode;
                        break;
                    case OnClassElement oce:
                        if (oce.Name == name)
                            return currentNode;
                        break;
                }
            }

            foreach (HierarchyNode node in currentNode.ChildNodes)
            {
                HierarchyNode sought = SeekNode(node, name, type);
                if (sought != null)
                    return sought;
            }

            return null;
        }

    }
}
