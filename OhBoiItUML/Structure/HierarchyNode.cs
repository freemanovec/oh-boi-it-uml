﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhBoiItUML.Structure
{
    internal sealed class HierarchyNode
    {
        public object Contained { get; private set; }
        public Type ContainedType { get; private set; }
        public List<HierarchyNode> ChildNodes { get; private set; } = new List<HierarchyNode>();

        public HierarchyNode(object contained)
        {
            Contained = contained;
            ContainedType = contained.GetType();
        }
    }
}
